from core.game_mechanics import initialize_game, run_game
from core.user_interface import UserInterface

from utils.load_config import load_config


def main():
    config = load_config()

    # Initialize the game
    player1, player2, platforms = initialize_game(config)

    user_interface = UserInterface(screen_width=config['screen']['width'],
                                   screen_height=config['screen']['height'])

    run_game(player1=player1,
             player2=player2,
             user_interface=user_interface,
             platforms=platforms,
             g=config['env']['g'])


if __name__ == '__main__':
    main()
