from core.entity import Entity


class Player(Entity):

    def __init__(self, x, y, jump, speed, width, height):
        """
        Player class that is to be controlled by the user. Currently can only move left and right.

        :param int x_coordinate: the x-position where the player is located
        :param int y_coordinate: fixed y-coordinate of the player
        :param int jump: how "hard" player can jump
        :param int speed: the horizontal distance the player can move per iteration
        :param int width: width of the player
        :param int height: height of the player
        """

        Entity.__init__(self, width=width, height=height, x=x, y=y)
        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.jump = jump
        self.speed = speed
        self.width = width
        self.height = height

    def update_position(self):

        self._update_coordinates()
        self._update_rectangle()

    def _update_coordinates(self):
        self.x += self.vx
        self.y += self.vy