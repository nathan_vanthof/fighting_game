import pygame

# Colours
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)


class UserInterface:

    def __init__(self, screen_width, screen_height):
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.screen = pygame.display.set_mode((screen_width, screen_height))

    def draw_screen(self, player1, player2, platforms):
        self.screen.fill(BLACK)

        for platform in platforms:
            pygame.draw.rect(self.screen, GREEN, platform.rectangle)

        pygame.draw.rect(self.screen, BLUE, player1.rectangle)
        pygame.draw.rect(self.screen, RED, player2.rectangle)

        pygame.display.flip()

