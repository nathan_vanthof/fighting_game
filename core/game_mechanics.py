import pygame
import time
import copy
import random

from core.entity import Entity
from core.player import Player
from core.controls import player1_controls, player2_controls
from core.user_inputs import check_player_quits


def initialize_game(config):
    player1 = Player(x=config['env']['player_1']['pos_x'],
                     y=config['env']['player_1']['pos_y'],
                     jump=config['player']['jump'],
                     speed=config['player']['speed'],
                     width=config['player']['width'],
                     height=config['player']['height'])

    player2 = Player(x=config['env']['player_2']['pos_x'],
                     y=config['env']['player_2']['pos_y'],
                     jump=config['player']['jump'],
                     speed=config['player']['speed'],
                     width=config['player']['width'],
                     height=config['player']['height'])

    platforms = []
    for platform in config['env']['platforms']:
        platforms.append(Entity(width=platform['width'],
                                height=platform['height'],
                                x=platform['x'],
                                y=platform['y']))

    return player1, player2, platforms


def check_game_running(player1, player2, screen_height):
    """
    Check if either of the players has gone below y = 0
    """

    running = True

    if player1.y > screen_height:
        print('player 1 died')
        running = False
    if player2.y > screen_height:
        print('player 2 died')
        running = False

    return running

def get_blocked_by_platform(player, platforms):
    # left top right bottom
    blocked = [0, 0, 0, 0]
    for platform in platforms:
        if platform.rectangle.colliderect(player.rectangle):
            player_top = player.y + player.height
            player_right = player.x + player.width

            if platform.y > player.y + player.height > platform.y + platform.height:
                blocked[1] = 1
                player.y = platform.y - player.height
            elif player_top > platform.y > player.y:
                blocked[3] = 1
                player.y = platform.y - player.height
            elif player.x < platform.x < player_right:
                blocked[0] = 1
                player.x = platform.x - player.width
            elif player.x < platform.x + platform.width < player_right:
                blocked[2] = 1
                player.x = platform.x + platform.width
    return blocked

def get_blocked(player1, player2, platforms):
    # blocked:
    # 0 is not at all
    # 1 is by platform
    # 2 is by other player
    # [left, top, right, bottom]
    player1_blocked = get_blocked_by_platform(player1, platforms)
    player2_blocked = get_blocked_by_platform(player2, platforms)
    if player1.rectangle.colliderect(player2.rectangle):
        if player1.y < player2.y + player2.height < player1.y + player1.height:
            player1_blocked[1] = 2
            player2_blocked[3] = 2
        elif player2.y < player1.y + player1.height < player2.y + player2.height:
            player1_blocked[3] = 2
            player2_blocked[1] = 2
        elif player1.x < player2.x < player1.x + player1.width:
            player1_blocked[0] = 2
            player2_blocked[2] = 2
        elif player2.x < player1.x < player2.x + player2.width:
            player1_blocked[2] = 2
            player2_blocked[0] = 2
    return player1_blocked, player2_blocked


def update_positions(player1, player2, platforms, g, input_1, input_2):
    blocked1, blocked2 = get_blocked(player1, player2, platforms)
    # jumping, moving
    if blocked1[-1] > 0:
        if input_1[1] > 0:
            player1.vy -= player1.jump * input_1[1]
        if input_1[-1] > 0.1:
            player1.vy = -abs(player1.vy)
        else:
            player1.vy = 0
        player1.vx += player1.speed * input_1[0]
    else:
        player1.vy += g

    if blocked2[-1] > 0:
        if input_2[1] > 0:
            player2.vy -= player2.jump * input_2[1]
        if input_2[-1] > 0.1:
            player2.vy = -abs(player2.vy)
        else:
            player2.vy = 0
        player2.vx += player2.speed * input_2[0]
    else:
        player2.vy += g

    # bumping
    if blocked1[1] > 0:
        player1.vy = abs(player1.vy)
        if blocked1[1] == 2:
            player2.vy -= 0.8 * player1.vy
            player2.vx += 0.1 * player1.vx + (random.random() - 1.) * 0.3
            player1.vx *= 0.9

    if blocked2[1] > 0:
        player2.vy = abs(player2.vy)
        if blocked2[1] == 2:
            player1.vy -= 0.8 * player2.vy
            player1.vx += 0.1 * player2.vx + (random.random() - 1.) * 0.3
            player2.vx *= 0.9

    if blocked1[0] > 0 and player1.vx < 0:
        if blocked1[0] == 2:
            [vx1, vx2] = sorted([player1.vx, player2.vx])
            player1.vx, player2.vx = 0.8 * vx1, 0.8 * vx2
        else:
            player1.vx = 0.8 * abs(player1.vx)

    if blocked2[0] > 0 and player2.vx < 0:
        if blocked2[0] == 2:
            vx_av = (player1.vx + player2.vx) / 2
            magnitude = abs(player1.vx) + abs(player2.vx)
            player1.vx = vx_av * 0.7 - magnitude / 4
            player2.vx = vx_av * 0.7 + magnitude / 4
        else:
            player1.vx = 0.8 * abs(player2.vx)

    if blocked1[0] > 0 and blocked1[2] > 0:
        player1.vx = 0
    if blocked1[1] > 0 and blocked1[3] > 0:
        player1.vy = 0

    if blocked2[0] > 0 and blocked2[2] > 0:
        player2.vx = 0
    if blocked2[1] > 0 and blocked2[3] > 0:
        player2.vy = 0

    player1.update_position()
    player2.update_position()


def run_game(player1, player2, user_interface, platforms, g):
    pygame.init()

    game_running = True

    while game_running:
        game_running = check_player_quits()
        if game_running:
            # Check if the game has been won/lost
            game_running = check_game_running(player1, player2, user_interface.screen_height)

        # Get user inputs
        input_1 = player1_controls((player1.x, player1.y), (player1.vx, player1.vy),
                                   (player2.x, player2.y), (player2.vx, player2.vy),
                                   (user_interface.screen_width, user_interface.screen_height),
                                   copy.deepcopy(platforms), g)
        input_1 = (min(1, max(-1, input_1[0])), min(1, max(-1, input_1[1])))
        input_2 = player2_controls((player2.x, player2.y), (player2.vx, player2.vy),
                                   (player1.x, player1.y), (player1.vx, player1.vy),
                                   (user_interface.screen_width, user_interface.screen_height),
                                   copy.deepcopy(platforms), g)
        input_2 = (min(1, max(-1, input_2[0])), min(1, max(-1, input_2[1])))

        update_positions(player1, player2, platforms, g, input_1, input_2)

        # Draw everything
        user_interface.draw_screen(player1=player1,
                                   player2=player2,
                                   platforms=platforms)

        # Sleep to get a 'standard' time interval
        time.sleep(0.02)

    pygame.quit()


