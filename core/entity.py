import pygame


class Entity:

    def __init__(self, width, height, x, y):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rectangle = pygame.Rect(x, y, width, height)

    def _update_rectangle(self):
        self.rectangle.x = self.x
        self.rectangle.y = self.y
