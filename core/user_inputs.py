import pygame
from pygame.locals import *


def check_player_quits():
    for event in pygame.event.get():
        if event.type == QUIT:
            return False

    key = pygame.key.get_pressed()
    if key[K_ESCAPE]:
        return False

    return True
