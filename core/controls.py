import random

def player1_controls(self_position, self_speed, other_position, other_speed, screen_dimensions, platforms, g):
    """

    :param self_position: (x, y)
    :param self_speed: (vx, vy)
    :param other_position: (x, y)
    :param other_speed: (vx, vy)
    :param screen_dimensions: (width, height)
    :param platforms: list of plaforms of the form[(x_center, y_center, width, height), .... ]
    :param g: gravity

    :return: tuple with values ranging from -1 to 1 eg: (-1, 1), ax, ay
    """

    current_platform = None
    for platform in platforms:
        if platform.x < self_position[0] < platform.x + platform.width:
            current_platform = platform
            break

    if current_platform is None:
        return (random.random() - 1., random.random() - 1.)

    from_edge = self_position[0] - current_platform.x
    if from_edge < current_platform.width / 2:
        direction = 1
    else:
        direction = -1

    if other_position[0] < self_position[0]:
        attack = -1
    else:
        attack = 1

    if attack * direction > 0 and self_speed[0] * direction > 0:
        if other_position[1] - self_position[1] > 5:
            return (direction, 0.5)
        return (direction, 0)
    if direction * self_speed[0] < 0:
        return (direction, -0.1)
    return (direction, 1)


def player2_controls(self_position, self_speed, other_position, other_speed, screen_dimensions, platforms, g):
    """

    :param self_position: (x, y)
    :param self_speed: (vx, vy)
    :param other_position: (x, y)
    :param other_speed: (vx, vy)
    :param screen_dimensions: (width, height)
    :param platforms: list of plaforms of the form[(x_center, y_center, width, height), .... ]
    :param g: gravity

    :return: tuple with values ranging from -1 to 1 eg: (-1, 1), ax, ay

    """

    x_1, y_1 = self_position
    x_2, y_2 = other_position
    vx_1, vy_1 = self_speed
    vx_2, vy_2 = other_speed

    while y_2 - 3 < y_1:
        x_1 += vx_1
        x_2 += vx_2
        vy_2 += g
        y_2 += vy_2

    if x_1 < x_2:
        return (0.5, 0)
    elif x_1 > x_2 + 10:
        return (-0.5, 0)

    return (self_position[0] < screen_dimensions[0] / 2, 0)

